﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrainsUrlRewriteHelper
{
    public class PageResult
    {
        public int strainId { get; set; }
        public string strainName { get; set; }
        public string type { get; set; }
    }

    public class RootObject
    {
        public List<PageResult> pageResults { get; set; }
    }
}
