﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace StrainsUrlRewriteHelper
{
    public class Program
    {
        private static readonly string STATIC_FILE_NAME_FORMAT = ConfigurationManager.AppSettings["StaticFileNameFormat"];
        private static readonly string STRAIN_TEMPLATE_FILE_PATH = ConfigurationManager.AppSettings["StrainTemplateFilePath"];
        private static readonly string HTACCESS_FILE_PATH = ConfigurationManager.AppSettings["HtAccessFilePath"];
        private static readonly string LAST_RUN_DATE_TIME_FILE_PATH = ConfigurationManager.AppSettings["LastRunDateTimeFilePath"];
        private static readonly char[] CharactersToOmitWhenConstructingUrl = new char[] { '(', ')', '%', '*', '\'', '#' };

        private static DateTime? LastExecutionTime
        {
            get
            {
                if(!File.Exists(LAST_RUN_DATE_TIME_FILE_PATH))
                    return null;

                DateTime lastExecutionTime;

                var fileContent = File.ReadAllText(LAST_RUN_DATE_TIME_FILE_PATH);
                if (!DateTime.TryParse(fileContent, out lastExecutionTime))
                    return null;

                return lastExecutionTime;
            }
        }

        public static void Main(string[] args)
        {
            IEnumerable<PageResult> strains;

            if (LastExecutionTime.HasValue)
                strains = GetStrainsUpdatedAfter(LastExecutionTime.Value).Result;
            else
                strains = GetStrains().Result;

            var newRewriteRules = GetRewriteRules(strains);
            ReplaceRewriteRules(newRewriteRules, strains.Select(s => s.strainId));

            var strainTemplate = File.ReadAllText(STRAIN_TEMPLATE_FILE_PATH);
            var directoryPath = new FileInfo(STRAIN_TEMPLATE_FILE_PATH).Directory.FullName;

            CreateStrainHtmlFiles(strains, strainTemplate, directoryPath);

            UpdateLastRunDateTime();
        }

        private static void UpdateLastRunDateTime()
        {
            File.WriteAllText(LAST_RUN_DATE_TIME_FILE_PATH, DateTime.Now.ToString());
        }

        private static void ReplaceRewriteRules(IEnumerable<string> newRules, IEnumerable<int> strainIds)
        {
            var htAccessLines = File.ReadAllLines(HTACCESS_FILE_PATH).ToList();
            var cannabisStrainsRulesStartLineIndex = htAccessLines.IndexOf(htAccessLines.FirstOrDefault(line => line.Contains("cannabis-strains")));
            htAccessLines = htAccessLines.Where(line => 
                strainIds.All(id => !line.Contains(String.Format(STATIC_FILE_NAME_FORMAT, id)))
            ).ToList();

            htAccessLines.InsertRange(cannabisStrainsRulesStartLineIndex, newRules);
            File.WriteAllLines(HTACCESS_FILE_PATH, htAccessLines);
        }

        private static void CreateStrainHtmlFiles(IEnumerable<PageResult> strains, string strainTemplate, string directoryPath)
        {
            var staticFilesDirectory = new DirectoryInfo(directoryPath);
            var staticFiles = staticFilesDirectory.GetFiles(String.Format(STATIC_FILE_NAME_FORMAT, "*"));

            var staticFileCreator = new StaticFileCreator(STATIC_FILE_NAME_FORMAT, strainTemplate, directoryPath, staticFiles);
            foreach (var strain in strains)
                staticFileCreator.CreateFileForStrain(strain);
        }

        private static IEnumerable<string> GetRewriteRules(IEnumerable<PageResult> strains)
        {
            var rules = new List<string> { };

            foreach (var strain in strains)
                rules.Add(new RewriteRuleCreator(strain, STATIC_FILE_NAME_FORMAT, CharactersToOmitWhenConstructingUrl).Create());
            
            return rules;
        }

        private static async Task<IEnumerable<PageResult>> GetStrainsUpdatedAfter(DateTime updatedAfter)
        {
            var strains = await new StrainsGetter().Get(updatedAfter).ConfigureAwait(false);
            return strains.Where(s =>
                !String.IsNullOrWhiteSpace(s.type) &&
                !String.IsNullOrWhiteSpace(s.strainName));
        }

        private static async Task<IEnumerable<PageResult>> GetStrains()
        {
            var strains = await new StrainsGetter().Get().ConfigureAwait(false);
            return strains.Where(s => 
                !String.IsNullOrWhiteSpace(s.type) && 
                !String.IsNullOrWhiteSpace(s.strainName));
        }
    }
}
