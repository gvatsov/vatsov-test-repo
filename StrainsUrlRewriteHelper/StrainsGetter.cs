﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StrainsUrlRewriteHelper
{
    public class StrainsGetter
    {
        private string RequestUrl(int pageIndex)
        {
            return String.Concat(ConfigurationManager.AppSettings["ApiUrl"], $"/strains/GetPage?page={pageIndex}&pageSize=1000");
        }

        private string RequestUrlForUpdated(string updatedAfter)
        {
            return String.Concat(ConfigurationManager.AppSettings["ApiUrl"], $"/strains/GetUpdatedAfter?updatedAfter={updatedAfter}");
        }

        public async Task<IEnumerable<PageResult>> Get()
        {
            var strains = Enumerable.Empty<PageResult>().ToList();
            var client = new HttpClient();

            var pageIndex = 1;
            while (true)
            {
                var response = await client.GetAsync(RequestUrl(pageIndex++)).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    var strainsPerPage = await response.Content.ReadAsAsync<RootObject>();
                    if (!strainsPerPage.pageResults.Any())
                        break;

                    strains.AddRange(strainsPerPage.pageResults);
                }
                else
                    return strains;
            }

            return strains;
        }

        public async Task<IEnumerable<PageResult>> Get(DateTime updatedAfter)
        {
            var client = new HttpClient();

            var updateAfterString = updatedAfter.ToString();
            var response = await client.GetAsync(RequestUrlForUpdated(updateAfterString)).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<IEnumerable<PageResult>>().Result;
            }
            else
                return Enumerable.Empty<PageResult>();
        }
    }
}
