﻿using System;
using System.IO;
using System.Linq;

namespace StrainsUrlRewriteHelper
{
    public class StaticFileCreator
    {
        private string _fileNameFormat;
        private string _strainTemplate;
        private string _directoryPath;
        private FileInfo[] _staticFiles;

        public StaticFileCreator(string fileNameFormat, string strainTemplate, string directoryPath, FileInfo[] staticFiles)
        {
            _fileNameFormat = fileNameFormat;
            _strainTemplate = strainTemplate;
            _directoryPath = directoryPath;
            _staticFiles = staticFiles;
        }

        public void CreateFileForStrain(PageResult strain)
        {
            var newFileContent = _strainTemplate.Replace("APP.Strain.init();", $"APP.Strain.init({strain.strainId});");

            var newFileName = String.Format(_fileNameFormat, strain.strainId);
            var newFileFullPath = Path.Combine(_directoryPath, newFileName);

            var existingFileWithThisName = _staticFiles.SingleOrDefault(file => file.Name == newFileName);
            if (existingFileWithThisName != null)
                existingFileWithThisName.Delete();

            File.WriteAllText(newFileFullPath, newFileContent);
        }
    }
}
