﻿using System;
using System.Text.RegularExpressions;

namespace StrainsUrlRewriteHelper
{
    public class RewriteRuleCreator
    {
        private PageResult _strain;
        private static string _staticPageStringFormat;
        private static Regex _beautifyRegex = new Regex(@"\s+", RegexOptions.Compiled);
        private static Regex _beautifySecondStepRegex = new Regex(@"[-]{2,}", RegexOptions.Compiled);
        private char[] _charactersToOmitWhenConstructingUrl;

        public RewriteRuleCreator(PageResult strain, string staticPageStringFormat, char[] charactersToOmitWhenConstructingUrl)
        {
            _strain = strain;
            _staticPageStringFormat = staticPageStringFormat;
            _charactersToOmitWhenConstructingUrl = charactersToOmitWhenConstructingUrl;
        }

        public string Create()
        {
            var beautifiedType = UrlBeautify(_strain.type);
            var beautifiedName = UrlBeautify(_strain.strainName);
            var newStaticPageName = String.Format(_staticPageStringFormat, _strain.strainId);

            var rewriteRule = $"RewriteRule ^cannabis-strains/{beautifiedType}/{beautifiedName} {newStaticPageName} [NC,L]";
            return rewriteRule;
        }

        private string UrlBeautify(string input)
        {
            var beautified = _beautifyRegex.Replace(input, "-").ToLower();
            beautified = _beautifySecondStepRegex.Replace(beautified, "-").ToLower();

            foreach (var character in _charactersToOmitWhenConstructingUrl)
                beautified = beautified.Replace(character.ToString(), string.Empty);

            beautified = beautified.TrimEnd('-');
            return beautified;
        }
    }
}
